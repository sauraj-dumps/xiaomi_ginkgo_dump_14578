#!/bin/bash

cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk.* 2>/dev/null >> cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk
rm -f cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk.* 2>/dev/null
cat cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null >> cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk
rm -f cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null
cat cust/app/customized/partner-video.like_111/partner-video.like_111.apk.* 2>/dev/null >> cust/app/customized/partner-video.like_111/partner-video.like_111.apk
rm -f cust/app/customized/partner-video.like_111/partner-video.like_111.apk.* 2>/dev/null
cat cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor/data-app/Duo/Duo.apk.* 2>/dev/null >> vendor/data-app/Duo/Duo.apk
rm -f vendor/data-app/Duo/Duo.apk.* 2>/dev/null
cat vendor/data-app/Photos/Photos.apk.* 2>/dev/null >> vendor/data-app/Photos/Photos.apk
rm -f vendor/data-app/Photos/Photos.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk.* 2>/dev/null >> system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk
rm -f system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk.* 2>/dev/null
cat system/system/priv-app/MiBrowserGlobal/MiBrowserGlobal.apk.* 2>/dev/null >> system/system/priv-app/MiBrowserGlobal/MiBrowserGlobal.apk
rm -f system/system/priv-app/MiBrowserGlobal/MiBrowserGlobal.apk.* 2>/dev/null
cat system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null >> system/system/priv-app/SecurityCenter/SecurityCenter.apk
rm -f system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null
cat system/system/product/app/Maps/Maps.apk.* 2>/dev/null >> system/system/product/app/Maps/Maps.apk
rm -f system/system/product/app/Maps/Maps.apk.* 2>/dev/null
cat system/system/product/app/Messages/Messages.apk.* 2>/dev/null >> system/system/product/app/Messages/Messages.apk
rm -f system/system/product/app/Messages/Messages.apk.* 2>/dev/null
cat system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null >> system/system/product/app/YouTube/YouTube.apk
rm -f system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/system/product/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/system/product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> system/system/product/app/Gmail2/Gmail2.apk
rm -f system/system/product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> system/system/product/priv-app/GmsCore/GmsCore.apk
rm -f system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
